# Briefing

For some context, this codebase contains my attempt to revise and improve on the initial EndGame code.
You'll find here a Docker-Compose structure that simulates a scaled service being deployed on TOR behind
OnionBalance and a custom version of EndGame. The sampled service is a small PHP/Laravel API I had at my
disposal (https://terseapi.com).

-   **EndGame codebase repository** (https://gitlab.com/vpalos/endgame-v3).
-   **Deployment simulation repository** (https://gitlab.com/vpalos/endgame-deploy)
    which includes the EndGame repository as a submodule.

Keep in mind that most of this is done in a rush and while learning a lot, so it should be seen as a
proof of concept, at best.

**Usage**:

```sh
# Prerequisites: Git, Docker, Docker-Compose.

# Clone both repos recursively.
git clone --recurse-submodules https://gitlab.com/vpalos/endgame-deploy

cd endgame-deploy
docker-compose up -d # Builds and starts all containers (takes a while).

# You can follow status with...
docker-compose logs -f
```

Once everything is built and starts-up, you'll have these Onion addresses (eventually) available to visit:

-   Balancer URL: [terse6q6aif7qeek6jryypdswwhgoy3cr44l4kx3jgp4s46xn3gvjkyd.onion](http://terse6q6aif7qeek6jryypdswwhgoy3cr44l4kx3jgp4s46xn3gvjkyd.onion)
    -   This takes >10m to properly publish all descriptors and actually become available.
-   EndGame-1: [terse2muhtjc5w66qo7ransjvdommz2kvkwjki3fsllh5ip2pxomtvyd.onion](http://terse2muhtjc5w66qo7ransjvdommz2kvkwjki3fsllh5ip2pxomtvyd.onion)
-   EndGame-2: [terse2vgebctrwh7uym2c4xe2koyo3rgse3zb4n233ngdj4qwpblrcyd.onion](http://terse2vgebctrwh7uym2c4xe2koyo3rgse3zb4n233ngdj4qwpblrcyd.onion)

Main changes are as follows...

## Dockerized EndGame

Code overhauled to allow running under Docker, restructured, fixed many issues to make it work, code
clean-up (to a certain extent, should be done more).

Any secrets that were customizable via `endgame.config` are now read from environment variables and
applied at container start-up, not during installation; this allows for safer deployments via CI/CD and
faster changes at run-time (network reconfiguration).

**Notes**

-   The whole setup process was overhauled;
-   OpenResty, Nginx, NAXSI (WAF) and Lua modules were all updated;
-   The original EndGame README.md does not reflect any of these changes (requires major rewrite).

**Todo's** (had no time for these):

-   Better management of TOR/OB start-up health states;
-   Handle log-rotation outside containers;
-   Manage fail2ban and cron better inside container.

## Redis storage instead of NGINX shared dictionaries

This enables all instances of EndGame to benefit from eachother's "experience" since cookie profiles
are instantly available to all nodes. Of course, this can incur a performance penalty since the latency
for accessing Redis is higher than a local NGINX shared dicrionary, but still it should be a manageable
compromise (also given that the Redis module recycles connections).

To inspect the data stored in Redis at run-time:

-   `docker-compose.yaml: redis_service`:
    -   [http://86.122.81.94:8001] - no authentication needed

**Notes**:

-   This is implemented as a very small module that decouples the Lua code from the storage implementation
    so we can change Redis with anything else without impacting the critical code bits.
-   This feature only becomes relevant for horizontally-scaled services, i.e. when cookies created
    on the same domain name, are shared between EndGame instances!

## Use M/L to detect abusive behavior

The main idea is to use a M/L classifier to mark suspicious sessions so that we can eventually:

1. Replace the ugly Captcha with an invisible Captcha so that we only use this Captcha to authenticate the
   session, **NOT TO DETECT BOTS**. I.e. the invisible Captcha would be enough to get a reasonable session
   identifier;
2. Use the M/L model to rank the session from benign (0) to abusive (1);
3. Take action (ban via centralized cookie dictionary or request TOR's PoW to increase required effort).

To achieve this, I envisioned the following steps:

1. **Store logs** at run-time in a side-db (MongoDB, Docker-deployed).
    - For start we can use the existing ban mechanism to mark sessions as GOOD/BAD for training data;
    - Eventually, we need a considerably better way to mark sessions (possibly manually) for training data.
    - `docker-compose.yaml: mongodb_express_service`:
        - [http://86.122.81.94:8081]
        - User: user
        - Pass: password
2. **Analyze logs to find a good model** that (at least) replicates the training data marks;
    - I intend to use [MindsDB](https://github.com/mindsdb/mindsdb) for this since it can analyze data on
      MongoDB directly, it can fine-tune models at run-time (retrain with additional data on-the-fly).
    - `docker-compose.yaml: mindsdb_service`:
        - [http://86.122.81.94:47334]
        - no authentication needed

> ---
>
> Altough this is the main change I intended to make, unfortunately it proved to significantly exceed the time and
> scope I had for this trial. So this is as far as I got while working on this initiative.
> 
> **Everything below this line is still in the works.**
>
> ---

3. **Run model as service** to queue and classify the sessions in MongoDB.
    - This would store the results in REDIS.
    - Classify new sessions (once authenticated) after ~5 seconds from first request or after ~5 requests
      are made (configurable thresholds, of course). The rank is saved in REDIS, where Nginx can use it!

Stores traffic data in Redis, grouped by cookie profile. This obviously relies upon the fact that sessions
are somehow authenticated, so that we can identify a user across a session of traffic.

We would then train models on this data and query them at run-time to discriminate incoming traffic.
Queries will asynchornously assign a _risk_ level to cookie sessions, using Lua co-routines (since these
might incur a performance penalty).

**Possible ideas for improvement/research**:

-   Integrate TOS's PoW: once the risk level is computed, sync PoW to adjust the required effort;
    this would require tapping-into the PoW code since currently the acan lgorithm only relies on some load
    monitoring metrics to compute challenges; we'd have to expose in Lua the means to adjust the algorithm
    or influence it with additional metrics. These will be useful for this research:

-   If traffic is huge, we could also save logs on a storage system compatible with big-data processing,
    such as AWS S3 where it can be easily probed/tested with tools like Apache Spark to test and validate
    M/L classifiers on a wider scale (this is only for finding reliable models).

**Notes**:

-   I just realised that [RedisAI](https://oss.redis.com/redisai/) would be a good (or even better?) alternative to MindsDB, needs checking.
-   These links will be useful as research in the context of this document:
    -   [PoW specification](https://spec.torproject.org/hspow-spec/index.html)
    -   [PoW Git merge request](https://gitlab.torproject.org/tpo/core/tor/-/merge_requests/702/diffs)
    -   [TOR Service Scaling](https://github.com/csucu/Tor-Hidden-Service-Scaling)
-   To inspect logged traffic, open [MongoDB Express](http://86.122.81.94:8081).
