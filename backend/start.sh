#!/bin/bash -xue

cd /var/www/html/backend

sed -i "s/^APP_URL=.*//g" .env
echo "APP_URL=\"${APP_URL:-http://localhost}\"" >>.env

php artisan cache:clear
php artisan config:clear
php artisan route:clear

# Start everything.
php-fpm8.1 -F &
nginx
